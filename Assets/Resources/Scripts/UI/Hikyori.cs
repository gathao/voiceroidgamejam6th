﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Hikyori : MonoBehaviour
{
    [SerializeField]
    Text hikyoriText = null;

    void Start()
    {
        
    }

    public void SetHikyori(float hikyori)
    {
        // 小数点第1位で切り捨て
        var showHikyori = Mathf.Floor(hikyori * 10.0f) / 10.0f;
        hikyoriText.text = string.Format("{0:0.0}", showHikyori);
    }
}
