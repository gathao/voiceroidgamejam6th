﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;


public class FadeUIItems : MonoBehaviour
{
    [SerializeField]
    RectTransform Left = null;

    [SerializeField]
    RectTransform Right = null;

    [SerializeField]
    RectTransform Top = null;

    [SerializeField]
    RectTransform Bottom = null;

    CanvasGroup leftCanvasGroup;

    CanvasGroup rightCanvasGroup;

    CanvasGroup topCanvasGroup;

    CanvasGroup bottomCanvasGroup;

    void Start()
    {
        leftCanvasGroup = Left.GetComponent<CanvasGroup>();
        rightCanvasGroup = Right.GetComponent<CanvasGroup>();
        topCanvasGroup = Top.GetComponent<CanvasGroup>();
        bottomCanvasGroup = Bottom.GetComponent<CanvasGroup>();
    }

    public void FadeInAll(float duration = 0.3f, bool isTransparent = true)
    {
        FadeInLeft(0.0f, duration, isTransparent);
        FadeInRight(0.0f, duration, isTransparent);
        FadeInTop(0.0f, duration, isTransparent);
        FadeInBottom(0.0f, duration, isTransparent);
    }

    public void FadeOutAll(float duration = 0.3f, bool isTransparent = true)
    {
        FadeOutLeft(0.0f, duration, isTransparent);
        FadeOutRight(0.0f, duration, isTransparent);
        FadeOutTop(0.0f, duration, isTransparent);
        FadeOutBottom(0.0f, duration, isTransparent);
    }

    public void FadeInLeft(float offset = 0.0f, float duration = 0.3f, bool isTransparent = true)
    {
        Left.SetLocalPositionX(-Screen.width + offset);
        Left.DOLocalMoveX(0.0f, duration);

        if (isTransparent)
        {
            leftCanvasGroup.alpha = 0.0f;
            leftCanvasGroup.DOFade(1.0f, duration);
        }
    }

    public void FadeOutLeft(float offset = 0.0f, float duration = 0.3f, bool isTransparent = true)
    {
        Left.SetLocalPositionX(0.0f);
        Left.DOLocalMoveX(-Screen.width + offset, duration);

        if (isTransparent)
        {
            leftCanvasGroup.alpha = 1.0f;
            leftCanvasGroup.DOFade(0.0f, duration);
        }
    }

    public void FadeInRight(float offset = 0.0f, float duration = 0.3f, bool isTransparent = true)
    {
        Right.SetLocalPositionX(Screen.width + offset);
        Right.DOLocalMoveX(0.0f, duration);

        if (isTransparent)
        {
            rightCanvasGroup.alpha = 0.0f;
            rightCanvasGroup.DOFade(1.0f, duration);
        }
    }

    public void FadeOutRight(float offset = 0.0f, float duration = 0.3f, bool isTransparent = true)
    {
        Right.SetLocalPositionX(0.0f);
        Right.DOLocalMoveX(Screen.width + offset, duration);

        if (isTransparent)
        {
            rightCanvasGroup.alpha = 1.0f;
            rightCanvasGroup.DOFade(0.0f, duration);
        }
    }

    public void FadeInTop(float offset = 0.0f, float duration = 0.3f, bool isTransparent = true)
    {
        Top.SetLocalPositionY(Screen.height + offset);
        Top.DOLocalMoveY(0.0f, duration);

        if (isTransparent)
        {
            topCanvasGroup.alpha = 0.0f;
            topCanvasGroup.DOFade(1.0f, duration);
        }
    }

    public void FadeOutTop(float offset = 0.0f, float duration = 0.3f, bool isTransparent = true)
    {
        Top.SetLocalPositionY(0.0f);
        Top.DOLocalMoveY(Screen.height + offset, duration);

        if (isTransparent)
        {
            topCanvasGroup.alpha = 1.0f;
            topCanvasGroup.DOFade(0.0f, duration);
        }
    }

    public void FadeInBottom(float offset = 0.0f, float duration = 0.3f, bool isTransparent = true)
    {
        Bottom.SetLocalPositionY(-Screen.height + offset);
        Bottom.DOLocalMoveY(0.0f, duration);

        if (isTransparent)
        {
            bottomCanvasGroup.alpha = 0.0f;
            bottomCanvasGroup.DOFade(1.0f, duration);
        }
    }

    public void FadeOutBottom(float offset = 0.0f, float duration = 0.3f, bool isTransparent = true)
    {
        Bottom.SetLocalPositionY(0.0f);
        Bottom.DOLocalMoveY(-Screen.height + offset, duration);

        if (isTransparent)
        {
            bottomCanvasGroup.alpha = 1.0f;
            bottomCanvasGroup.DOFade(0.0f, duration);
        }
    }
}
