﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BattingInfo : MonoBehaviour
{
    [SerializeField]
    Text nokoriBallNumText = null;

    [SerializeField]
    Text targetHomerunText = null;

    [SerializeField]
    Text homerunCountText = null;

    [SerializeField]
    Text homerunClearText = null;

    [SerializeField]
    Text targetHikyoriText = null;

    [SerializeField]
    Text hikyoriText = null;

    [SerializeField]
    Text hikyoriClearText = null;

    int nokoriBallNum = 10;

    int targetHomerun = 0;

    int homerunCount = 0;

    float targetHikyori = 0.0f;

    float hikyori = 0.0f;

    public int NokoriBallNum
    {
        get { return nokoriBallNum; }
        set
        {
            nokoriBallNum = value;
            nokoriBallNumText.text = nokoriBallNum.ToString();
        }
    }

    public int TargetHomerun
    {
        get { return targetHomerun; }
        set
        {
            targetHomerun = value;
            targetHomerunText.text = targetHomerun.ToString();
        }
    }

    public int HomerunCount
    {
        get { return homerunCount; }
        set
        {
            homerunCount = value;
            homerunCountText.text = homerunCount.ToString();

            // クリア判定
            if (targetHomerun <= homerunCount)
            {
                homerunClearText.gameObject.SetActive(true);
            }
        }
    }

    public float TargetHikyori
    {
        get { return targetHikyori; }
        set
        {
            targetHikyori = value;
            var showHikyori = Mathf.Floor(targetHikyori * 10.0f) / 10.0f;
            targetHikyoriText.text = string.Format("{0:0.0}", showHikyori);
        }
    }

    public float Hikyori
    {
        get { return hikyori; }
        set
        {
            hikyori = value;
            var showHikyori = Mathf.Floor(hikyori * 10.0f) / 10.0f;
            hikyoriText.text = string.Format("{0:0.0}", showHikyori);

            // クリア判定
            if (targetHikyori <= hikyori)
            {
                hikyoriClearText.gameObject.SetActive(true);
            }

        }
    }

    void Start()
    {
        
    }

    public void ResetCounts()
    {
        HomerunCount = 0;
        Hikyori = 0.0f;
        homerunClearText.gameObject.SetActive(false);
        hikyoriClearText.gameObject.SetActive(false);
    }
}
