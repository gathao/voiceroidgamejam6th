﻿using System;
using System.Collections.Generic;
using UniRx;
using UnityConstants;
using UnityEngine;
using UnityEngine.UI;

public class LevelSelectButton : MonoBehaviour
{
    [SerializeField]
    public Button button = null;

    [SerializeField]
    GameObject grayMask = null;

    [SerializeField]
    Image charaImage = null;

    [SerializeField]
    Image levelTitleImage = null;

    List<IDisposable> disposables = new List<IDisposable>();

    void Start()
    {
        Observable
            .EveryFixedUpdate()
            .Where(_ => charaImage != null && levelTitleImage != null)
            .Subscribe(_ =>
            {
                charaImage.color = button.targetGraphic.canvasRenderer.GetColor();
                levelTitleImage.color = button.targetGraphic.canvasRenderer.GetColor();
            })
            .AddTo(disposables);
    }

    public void ChangeActive(bool isActive)
    {
        button.interactable = isActive;
        grayMask.SetActive(!isActive);
    }

    public void SelectedColor()
    {
        button.image.color = new Color(0.9339623f, 0.8674635f, 0.6123621f);
    }

    public void NonSelectColor()
    {
        button.image.color = new Color(1.0f, 1.0f, 1.0f);
    }

    void OnDestroy()
    {
        foreach (var val in disposables)
        {
            val.Dispose();
        }
        disposables.Clear();
    }
}
