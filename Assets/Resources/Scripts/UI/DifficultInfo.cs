﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DifficultInfo : MonoBehaviour
{
    [SerializeField]
    RectTransform NonSelect = null;

    [SerializeField]
    RectTransform Selected = null;

    [SerializeField]
    RectTransform Level01 = null;

    [SerializeField]
    RectTransform Level02 = null;

    [SerializeField]
    RectTransform Level03 = null;

    [SerializeField]
    RectTransform Level04 = null;

    CanvasGroup canvasGroup01;

    CanvasGroup canvasGroup02;

    CanvasGroup canvasGroup03;

    CanvasGroup canvasGroup04;

    RectTransform currentLevel = null;

    CanvasGroup currentCanvasGroup = null;

    void Start()
    {
        canvasGroup01 = Level01.GetComponent<CanvasGroup>();
        canvasGroup02 = Level02.GetComponent<CanvasGroup>();
        canvasGroup03 = Level03.GetComponent<CanvasGroup>();
        canvasGroup04 = Level04.GetComponent<CanvasGroup>();

        NonSelect.gameObject.SetActive(true);
        Selected.gameObject.SetActive(false);
        Level01.gameObject.SetActive(false);
        Level02.gameObject.SetActive(false);
        Level03.gameObject.SetActive(false);
        Level04.gameObject.SetActive(false);
    }

    public void ChangeLevelInfo(PitcherLevel level)
    {
        NonSelect.gameObject.SetActive(false);
        Selected.gameObject.SetActive(true);

        if (currentLevel != null && currentCanvasGroup != null)
        {
            currentLevel.SetLocalPositionX(0.0f);
            currentLevel.DOLocalMoveX(50.0f, 0.3f);

            currentCanvasGroup.alpha = 1.0f;
            currentCanvasGroup.DOFade(0.0f, 0.3f);
        }

        switch (level)
        {
            case PitcherLevel.None:
                break;

            case PitcherLevel.Level01:
                Level01.gameObject.SetActive(true);

                Level01.SetLocalPositionX(-50.0f);
                Level01.DOLocalMoveX(0.0f, 0.3f);

                canvasGroup01.alpha = 0.0f;
                canvasGroup01.DOFade(1.0f, 0.3f);

                currentLevel = Level01;
                currentCanvasGroup = canvasGroup01;
                break;

            case PitcherLevel.Level02:
                Level02.gameObject.SetActive(true);

                Level02.SetLocalPositionX(-50.0f);
                Level02.DOLocalMoveX(0.0f, 0.3f);

                canvasGroup02.alpha = 0.0f;
                canvasGroup02.DOFade(1.0f, 0.3f);

                currentLevel = Level02;
                currentCanvasGroup = canvasGroup02;

                break;
            case PitcherLevel.Level03:
                Level03.gameObject.SetActive(true);

                Level03.SetLocalPositionX(-50.0f);
                Level03.DOLocalMoveX(0.0f, 0.3f);

                canvasGroup03.alpha = 0.0f;
                canvasGroup03.DOFade(1.0f, 0.3f);

                currentLevel = Level03;
                currentCanvasGroup = canvasGroup03;
                break;

            case PitcherLevel.Level04:
                Level04.gameObject.SetActive(true);

                Level04.SetLocalPositionX(-50.0f);
                Level04.DOLocalMoveX(0.0f, 0.3f);

                canvasGroup04.alpha = 0.0f;
                canvasGroup04.DOFade(1.0f, 0.3f);

                currentLevel = Level04;
                currentCanvasGroup = canvasGroup04;
                break;
        }
    }
}

