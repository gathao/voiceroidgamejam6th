﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelText : MonoBehaviour
{
    [SerializeField]
    Image level01 = null;

    [SerializeField]
    Image level02 = null;

    [SerializeField]
    Image level03 = null;

    [SerializeField]
    Image level04 = null;

    void Start()
    {
        level01.gameObject.SetActive(true);
        level02.gameObject.SetActive(false);
        level03.gameObject.SetActive(false);
        level04.gameObject.SetActive(false);
    }

    public void ChangeLevelText(PitcherLevel level)
    {
        switch(level)
        {
            case PitcherLevel.Level01:
                level01.gameObject.SetActive(true);
                level02.gameObject.SetActive(false);
                level03.gameObject.SetActive(false);
                level04.gameObject.SetActive(false);
                break;
            case PitcherLevel.Level02:
                level01.gameObject.SetActive(false);
                level02.gameObject.SetActive(true);
                level03.gameObject.SetActive(false);
                level04.gameObject.SetActive(false);
                break;
            case PitcherLevel.Level03:
                level01.gameObject.SetActive(false);
                level02.gameObject.SetActive(false);
                level03.gameObject.SetActive(true);
                level04.gameObject.SetActive(false);
                break;
            case PitcherLevel.Level04:
                level01.gameObject.SetActive(false);
                level02.gameObject.SetActive(false);
                level03.gameObject.SetActive(false);
                level04.gameObject.SetActive(true);
                break;
        }
    }
}
