﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class BattingStartButton : MonoBehaviour
{
    [SerializeField]
    public Button startButton = null;

    [SerializeField]
    Animator animator = null;

    public bool IsActive { get; private set; } = false;

    void Start()
    {
        animator.SetInteger("animIdx", 0);
    }

    public void HighrightButton()
    {
        if (IsActive)
        {
            return;
        }

        animator.SetInteger("animIdx", 1);

        IsActive = true;
    }

    public void StartAnimation()
    {
        startButton.enabled = false;
        animator.SetInteger("animIdx", 2);
    }
}
