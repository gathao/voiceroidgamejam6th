﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System.Threading;
using UnityEngine.UI;

public class BattingFonts : MonoBehaviour
{
    [SerializeField]
    SpriteRenderer hitText= null;

    [SerializeField]
    SpriteRenderer homerunText = null;

    [SerializeField]
    SpriteRenderer gwaragowagakinText = null;

    [SerializeField]
    SpriteRenderer strikeText = null;

    [SerializeField]
    SpriteRenderer fauleText = null;

    [SerializeField]
    RectTransform playballText = null;

    [SerializeField]
    RectTransform gamesetText = null;

    bool isPlaying = false;

    Vector3 endScale = new Vector3(72.0f, 72.0f, 72.0f);

    void Start()
    {
        
    }

    public void Hit()
    {
        if (isPlaying)
        {
            return;
        }

        DOTween.Sequence()
            .OnStart(() =>
            {
                isPlaying = true;
                hitText.transform.SetLocalScale(2.0f, 2.0f, 2.0f);
                hitText.gameObject.SetActive(true);
                hitText.transform.DOScale(endScale, 0.5f);
            })
            .AppendInterval(1.0f)
            .AppendCallback(() =>
            {
                isPlaying = false;
                hitText.gameObject.SetActive(false);
            })
            .Play();
    }

    public void Homerun()
    {
        if (isPlaying)
        {
            return;
        }

        DOTween.Sequence()
            .OnStart(() =>
            {
                isPlaying = true;
                homerunText.transform.SetLocalScale(2.0f, 2.0f, 2.0f);
                homerunText.gameObject.SetActive(true);
                homerunText.transform.DOScale(endScale, 0.3f);
            })
            .AppendInterval(1.5f)
            .AppendCallback(() =>
            {
                isPlaying = false;
                homerunText.gameObject.SetActive(false);
            })
            .Play();
    }

    public void Gwaragowagakin()
    {
        if (isPlaying)
        {
            return;
        }

        DOTween.Sequence()
            .OnStart(() =>
            {
                isPlaying = true;
                gwaragowagakinText.gameObject.SetActive(true);

                gwaragowagakinText.transform.SetLocalScale(2.0f, 2.0f, 2.0f);
                gwaragowagakinText.transform.DOScale(endScale, 0.1f);
                gwaragowagakinText.transform.DOShakePosition(1.0f, 35.0f);
            })
            .AppendInterval(1.5f)
            .AppendCallback(() =>
            {
                isPlaying = false;
                gwaragowagakinText.gameObject.SetActive(false);
            })
            .Play();
    }

    public void Strike()
    {
        if (isPlaying)
        {
            return;
        }

        DOTween.Sequence()
            .OnStart(() =>
            {
                isPlaying = true;

                AudioManager.PlaySe(GameSE.Stryke);
                strikeText.transform.SetLocalScale(2.0f, 2.0f, 2.0f);
                strikeText.gameObject.SetActive(true);
                strikeText.transform.DOScale(endScale, 0.25f);
            })
            .AppendInterval(1.0f)
            .AppendCallback(() =>
            {
                isPlaying = false;
                strikeText.gameObject.SetActive(false);
            })
            .Play();
    }

    public void Faule()
    {
        if (isPlaying)
        {
            return;
        }

        DOTween.Sequence()
            .OnStart(() =>
            {
                isPlaying = true;

                AudioManager.PlaySe(GameSE.Result_Faul);
                fauleText.transform.SetLocalScale(2.0f, 2.0f, 2.0f);
                fauleText.gameObject.SetActive(true);
                fauleText.transform.DOScale(endScale, 0.5f);
            })
            .AppendInterval(1.0f)
            .AppendCallback(() =>
            {
                isPlaying = false;
                fauleText.gameObject.SetActive(false);
            })
            .Play();
    }

    public void Playball()
    {
        if (isPlaying)
        {
            return;
        }

        DOTween.Sequence()
            .OnStart(() =>
            {
                isPlaying = true;

                AudioManager.PlaySe(GameSE.Playball);
                playballText.DOPivotX(2.5f, 0.0f);
                playballText.gameObject.SetActive(true);
                playballText.DOPivotX(0.5f, 0.3f);
            })
            .AppendInterval(1.3f)
            .AppendCallback(() =>
            {
                playballText.SetPositionX(1000.0f);
                playballText.DOPivotX(-1.5f, 0.3f);
            })
            .AppendInterval(0.5f)
            .AppendCallback(() =>
            {
                isPlaying = false;
                playballText.gameObject.SetActive(false);
            })
            .Play();
    }

    public void Gameset()
    {
        DOTween.Sequence()
            .OnStart(() =>
            {
                isPlaying = true;

                gamesetText.DOPivotX(2.5f, 0.0f);
                gamesetText.gameObject.SetActive(true);
                gamesetText.DOPivotX(0.5f, 0.3f);
            })
            .AppendInterval(1.3f)
            .AppendCallback(() =>
            {
                gamesetText.SetPositionX(1000.0f);
                gamesetText.DOPivotX(-1.5f, 0.3f);
            })
            .AppendInterval(0.5f)
            .AppendCallback(() =>
            {
                isPlaying = false;
                gamesetText.gameObject.SetActive(false);
            })
            .Play();
    }
}
