﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using UniRx.Triggers;
using UnityConstants;
using DG.Tweening;
using UnityEngine.UI;

public class BattingSceneManager : MonoBehaviour
{
    [SerializeField]
    Collider2D strikeCollider = null;

    [SerializeField]
    Collider2D hitCollider = null;

    [SerializeField]
    GameObject criticalPoint = null;

    [SerializeField]
    BattingFonts battingFonts = null;

    [SerializeField]
    PitcherController pitcher = null;

    [SerializeField]
    ThrowBall throwBall = null;

    [SerializeField]
    HitedBall hitedBall = null;

    [SerializeField]
    SpriteRenderer hittedBackground = null;

    [SerializeField]
    GameCameraController mainCamera = null;

    [SerializeField]
    GameCameraController hittedCamera = null;

    [SerializeField]
    BattingPlayerController player = null;

    [SerializeField]
    Hikyori hikyori = null;

    [SerializeField]
    EffectController effectCtrl = null;

    [SerializeField]
    Button pauseButton = null;

    [SerializeField]
    PauseManager pauseManager = null;

    [SerializeField]
    BattingInfo battingInfo = null;

    [SerializeField]
    LevelText levelText = null;

    public bool IsBattingScene { get; private set; } = true;

    public bool IsGameSet { get; set; } = false;

    public bool IsCriticalHit { get; set; } = false;

    List < IDisposable > disposables = new List<IDisposable>();

    List<Sequence> sequences = new List<Sequence>();


    void Start()
    {
        mainCamera.SetFixedMode();

        hittedBackground.gameObject.SetActive(false);
        mainCamera.gameObject.SetActive(true);
        hittedCamera.gameObject.SetActive(false);

        AudioManager.PlayBgm(GameBGM.Stage03);

        // 目標数をセット
        Observable
            .EveryFixedUpdate()
            .Skip(1)
            .Take(1)
            .Subscribe(_ =>
            {
                InitBattingInfo();
            });

        // ゲームセット判定
        Observable
            .EveryFixedUpdate()
            .Where(_ => IsGameSet)
            .Take(1)
            .Subscribe(_ =>
            {
                var gamesetSeq = DOTween.Sequence()
                    .OnStart(() =>
                    {
                        AudioManager.PlaySe(GameSE.GameSet);
                        battingFonts.Gameset();
                    })
                    .AppendInterval(1.8f)
                    .AppendCallback(() =>
                    {
                        // 試合結果記録
                        AppManager.Instance.LastBattingInfo = battingInfo;

                        // リザルト画面へ
                        AppManager.Instance.ChangeScene(SceneNames.ResultScene);
                    })
                    .Play();

                sequences.Add(gamesetSeq);
            });

        Observable
            .TimerFrame(60, FrameCountType.FixedUpdate)
            .Subscribe(_ =>
            {
                AudioManager.PlaySe(GameSE.Playball);
                battingFonts.Playball();
                pitcher.Throw();
            })
            .AddTo(disposables);

        strikeCollider
            .OnTriggerEnter2DAsObservable()
            .Where(x => x.gameObject.tag == Tags.Ball)
            .Subscribe(_ =>
            {
                var strikeSeq = DOTween.Sequence()
                    .OnStart(() =>
                    {
                        AudioManager.PlaySe(GameSE.Result_Faul);
                        battingFonts.Strike();
                        player.IsPlayable = false;
                        IsBattingScene = false;
                    })
                    .AppendInterval(2.0f)
                    .AppendCallback(() =>
                    {
                        battingInfo.NokoriBallNum = Math.Max(battingInfo.NokoriBallNum - 1, 0);
                        IsBattingScene = true;

                        if (battingInfo.NokoriBallNum == 0)
                        {
                            IsGameSet = true;
                            pitcher.Wait();
                        }
                        else
                        {
                            pitcher.Wait();
                            pitcher.Throw();
                            player.IsPlayable = true;
                        }
                    })
                    .Play();

                sequences.Add(strikeSeq);
            })
            .AddTo(disposables);

        hitCollider
            .OnTriggerEnter2DAsObservable()
            .Where(x => x.gameObject.tag == Tags.Ball)
            .Subscribe(_ =>
            {
                hitCollider.gameObject.SetActive(false);
                player.IsPlayable = false;
                HitProcess();
            })
            .AddTo(disposables);

        pauseButton.onClick.AsObservable()
            .Subscribe(_ =>
            {
                AudioManager.PlaySe(GameSE.CursorEnter);
                pauseManager.PauseAll();
            })
            .AddTo(disposables);
    }

    void InitBattingInfo()
    {
        battingInfo.ResetCounts();

        var level = AppManager.Instance.PitcherLevel;

        switch (level)
        {
            case PitcherLevel.Level01:
                battingInfo.NokoriBallNum = 10;
                battingInfo.TargetHomerun = 4;
                battingInfo.TargetHikyori = 1300.0f;
                break;
            case PitcherLevel.Level02:
                battingInfo.NokoriBallNum = 12;
                battingInfo.TargetHomerun = 6;
                battingInfo.TargetHikyori = 1600.0f;
                break;
            case PitcherLevel.Level03:
                battingInfo.NokoriBallNum = 15;
                battingInfo.TargetHomerun = 8;
                battingInfo.TargetHikyori = 1800.0f;
                break;
            case PitcherLevel.Level04:
                battingInfo.NokoriBallNum = 18;
                battingInfo.TargetHomerun = 12;
                battingInfo.TargetHikyori = 2400.0f;
                break;
        }

        levelText.ChangeLevelText(level);
    }

    void HitProcess()
    {
        // クリティカルポイントとの距離
        var criticalDistance = Vector2.Distance(throwBall.transform.position, criticalPoint.transform.position);

        // パワーと角度
        float hitPower = 2.9025f;
        float rad = 0.0f;

        var xDistance = throwBall.transform.position.x - criticalPoint.transform.position.x;
        var yDistance = throwBall.transform.position.y - criticalPoint.transform.position.y;

        // パワー
        hitPower = 1.4f * (1.0f - Mathf.Sin(criticalDistance) / 0.6f);
        hitPower += (0.7f - Mathf.Abs(yDistance)) / 0.10f;
        hitPower = Mathf.Max(hitPower, 3.5f);

        // 角度
        rad = Mathf.Clamp(xDistance / 0.28f, -1.1f, 1.1f);

        // クリティカルヒット
        if (criticalDistance < 0.13f)
        {
            var criticalSeq = DOTween.Sequence()
                .OnStart(() =>
                {
                    IsCriticalHit = true;

                    throwBall.HitStop();
                    effectCtrl.PlayEffect(EffectName.MagicHit, criticalPoint.transform.position);
                    TimeManager.Instance.SlowDown(0.0f, 0.1f);
                })
                .AppendInterval(0.1f)
                .AppendCallback(() =>
                {
                    hittedCamera.Shake();
                    battingFonts.Gwaragowagakin();
                    AudioManager.PlaySe(GameSE.Critical_Metal);
                    throwBall.HitFlyies();
                })
                .AppendInterval(0.3f)
                .AppendCallback(() =>
                {
                    hitedBall.CriticalBall(hitPower, rad);
                    ChangeFlyingCamera();
                })
                .Play();

            sequences.Add(criticalSeq);
        }
        else
        {
            hitedBall.HitBall(hitPower, rad);
            AudioManager.PlaySe(GameSE.Hit_Metal);
            throwBall.HitFlyies();
            ChangeFlyingCamera();
        }
    }

    public void ChangeBattingCamera()
    {
        IsCriticalHit = false;
        IsBattingScene = true;
        hikyori.gameObject.SetActive(false);
        mainCamera.gameObject.SetActive(true);
        hittedCamera.gameObject.SetActive(false);
        hittedBackground.gameObject.SetActive(false);

        if (battingInfo.NokoriBallNum == 0)
        {
            IsGameSet = true;
            pitcher.Wait();
        }
        else
        {
            pitcher.Wait();
            pitcher.Throw();

            player.LiftAnimation();
        }
    }

    public void ChangeFlyingCamera()
    {
        IsBattingScene = false;
        hikyori.gameObject.SetActive(true);
        hittedBackground.gameObject.SetActive(true);
        mainCamera.gameObject.SetActive(false);
        hittedCamera.gameObject.SetActive(true);
        hittedCamera.ResetPosition();
    }

    void OnDestroy()
    {
        foreach (var val in disposables)
        {
            val.Dispose();
        }
        disposables.Clear();

        foreach (var seq in sequences)
        {
            seq.Kill();
        }
        sequences.Clear();
    }
}
