﻿using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using DG.Tweening;

public class HitedBall : MonoBehaviour
{
    [SerializeField]
    BattingSceneManager battingSceneManager = null;

    [SerializeField]
    SpriteRenderer hitedBall = null;

    [SerializeField]
    SpriteRenderer ballShadow = null;

    [SerializeField]
    BattingFonts battingFonts = null;

    float dx = 0.0f;

    float dy = 0.0f;

    float dz = 0.0f;

    [SerializeField]
    float defaultDx = 0.17f;

    [SerializeField]
    float defaultDy = 0.17f;

    [SerializeField]
    float defaultDz = 0.08f;

    [SerializeField]
    float gravity = 0.000004f;

    [SerializeField]
    float resistance = 0.007f;

    float shadowDx = 0.0f;
    float shadowDy = 0.0f;

    bool isFlying = false;

    [SerializeField]
    Hikyori hikyori = null;

    float distance = 0.0f;

    [SerializeField]
    BattingInfo battingInfo = null;

    List<IDisposable> disposables = new List<IDisposable>();

    void Start()
    {
        hitedBall.color = new Color(hitedBall.color.r, hitedBall.color.g, hitedBall.color.b, 0.0f);
        ballShadow.color = new Color(ballShadow.color.r, ballShadow.color.g, ballShadow.color.b, 0.0f);

        PositionReset();

        Observable
            .EveryFixedUpdate()
            .Where(_ => battingSceneManager != null)
            .Where(_ => isFlying)
            .Subscribe(_ =>
            {
                dx *= resistance;
                dy *= resistance;

                dz = Mathf.Max(dz - gravity, -0.018f);
                dy += dz;

                shadowDx *= resistance;
                shadowDy *= resistance;

                hitedBall.transform.AddPosition(dx, dy, 0.0f);
                ballShadow.transform.AddPosition(shadowDx, shadowDy, 0.0f);

                // 飛距離計算
                distance = Vector2.Distance(ballShadow.transform.position, new Vector2(0.0f, -5.0f)) * 10.0f;
                hikyori.SetHikyori(distance);
            })
            .AddTo(disposables);
    }

    void PositionReset()
    {
        hitedBall.transform.SetPosition(0.0f, -5.0f, 0.0f);
        ballShadow.transform.SetPosition(0.0f, -5.0f, 0.0f);
    }

    public void CriticalBall(float power = 1.8f, float rad = 0.0f)
    {
        HitBall(power * 1.05f, rad);
    }

    // power: 2.5 ～ 2.9025f ～ 7.5
    // deg : -1.1f ～ 0.0f ～ 1.1f
    public void HitBall(float power = 1.8f, float rad = 0.0f)
    {
        isFlying = true;
        hitedBall.color = new Color(hitedBall.color.r, hitedBall.color.g, hitedBall.color.b, 1.0f);
        ballShadow.color = new Color(ballShadow.color.r, ballShadow.color.g, ballShadow.color.b, 0.5490196f);

        PositionReset();

        dx = defaultDx * power * Mathf.Sin(rad) * 0.7f;
        dy = defaultDy * power * Mathf.Cos(rad) * 0.7f;
        dz = defaultDz * power * 0.3f * 0.5f;

        shadowDx = dx;
        shadowDy = dy;

        // 着地判定
        Observable
            .EveryFixedUpdate()
            .Where(_ => isFlying)
            .Where(_ => (hitedBall.transform.position.y - ballShadow.transform.position.y) < 0.0f)
            .Skip(1)
            .Take(1)
            .Subscribe(_ =>
            {
                DOTween.Sequence()
                    .OnStart(() =>
                    {
                        isFlying = false;

                        HitedResult(rad);

                        hitedBall.color = new Color(hitedBall.color.r, hitedBall.color.g, hitedBall.color.b, 0.0f);
                        ballShadow.color = new Color(ballShadow.color.r, ballShadow.color.g, ballShadow.color.b, 0.0f);
                    })
                    .AppendInterval(2.0f)
                    .AppendCallback(() =>
                    {
                        battingInfo.NokoriBallNum = Math.Max(battingInfo.NokoriBallNum - 1, 0);
                        battingSceneManager.ChangeBattingCamera();
                    })
                    .Play();
            })
            .AddTo(disposables);
    }

    // 打球判定
    public void HitedResult(float rad)
    {
        // ファール
        if (rad <= -0.9f || 0.9f <= rad)
        {
            AudioManager.PlaySe(GameSE.Result_Faul);
            battingFonts.Faule();
        }
        // ホームラン
        else if (150.0f <= distance)
        {
            AudioManager.PlaySe(GameSE.Result_Homerun);
            battingFonts.Homerun();

            battingInfo.Hikyori += distance;
            battingInfo.HomerunCount++;
        }
        // ヒット
        else
        {
            AudioManager.PlaySe(GameSE.Result_Hit);
            battingFonts.Hit();

            battingInfo.Hikyori += distance;
        }
    }

    void OnDestroy()
    {
        foreach(var val in disposables)
        {
            val.Dispose();
        }
        disposables.Clear();
    }
}
