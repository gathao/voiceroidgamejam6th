﻿using System;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public enum PitcherLevel
{
    None = 0,
    Level01 = 1,
    Level02 = 2,
    Level03 = 3,
    Level04 = 4,
}

public class PitcherController : MonoBehaviour
{
    [SerializeField]
    ThrowBall throwBall = null;

    [SerializeField]
    Animator pitcherAnimator = null;

    [SerializeField]
    public PitcherLevel pitcherLevel = PitcherLevel.Level01;

    float seTime = 3.6f;

    float throwTime = 0.2f;

    static readonly Dictionary<PitcherLevel, ThrowBallProcBase> ThrowBallList = new Dictionary<PitcherLevel, ThrowBallProcBase>()
    {
        { PitcherLevel.Level01, new ThrowProc_Level01() },
        { PitcherLevel.Level02, new ThrowProc_Level02() },
        { PitcherLevel.Level03, new ThrowProc_Level03() },
        { PitcherLevel.Level04, new ThrowProc_Level04() },
    };

    List<Sequence> sequences = new List<Sequence>();

    void Start()
    {
        pitcherLevel = AppManager.Instance.PitcherLevel;

        ThrowBallProcBase throwBallProc = null;
        if (!ThrowBallList.TryGetValue(pitcherLevel, out throwBallProc))
        {
            throwBallProc = new ThrowProc_Level01();
        }

        throwBallProc.Setup(throwBall);
        throwBall.Setup(throwBallProc);

        Wait();
    }

    public void Wait()
    {
        pitcherAnimator.SetInteger("AnimIdx", 0);
    }

    public void Throw()
    {
        var throwSeq = DOTween.Sequence()
            .OnStart(() =>
            {
                pitcherAnimator.SetInteger("AnimIdx", 1);
            })
            .AppendInterval(seTime)
            .AppendCallback(() =>
            {
                AudioManager.PlaySe(GameSE.Throw);
            })
            .AppendInterval(throwTime)
            .AppendCallback(() =>
            {
                throwBall.Throw();
            })
            .Play();

        sequences.Add(throwSeq);
    }

    void OnDestroy()
    {
        foreach (var seq in sequences)
        {
            seq.Kill();
        }
        sequences.Clear();
    }
}
