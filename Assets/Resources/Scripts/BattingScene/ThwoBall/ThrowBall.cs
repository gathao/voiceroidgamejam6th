﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using System;

public class ThrowBall : MonoBehaviour
{
    [SerializeField]
    BattingSceneManager battingSceneManager = null;

    [SerializeField]
    public Rigidbody2D rigidbody2D = null;

    [SerializeField]
    public GameObject sprites = null;

    ThrowBallProcBase throwProc;

    List<IDisposable> disposables = new List<IDisposable>();

    void Start()
    {
        // ボールのY軸位置に応じて拡大率を変える
        Observable
            .EveryUpdate()
            .Where(_ => battingSceneManager != null)
            .Subscribe(_ =>
            {
                // y = 10x - 4.5
                // x = (y + 4.5) / 10
                float scale = 1.1f - ((transform.position.y + 4.5f) / 10.0f);
                scale = Mathf.Min(scale, 0.8f);
                transform.SetLocalScale(scale, scale, 1.0f);
            })
            .AddTo(disposables);
    }

    public void Setup(ThrowBallProcBase throwProc)
    {
        this.throwProc = throwProc;
    }

    public void Throw()
    {
        throwProc.RandomThrow();
    }

    public void HitFlyies()
    {
        rigidbody2D.velocity = new Vector2(0.0f, 0.0f);
        rigidbody2D.AddForce(new Vector2(0.0f, 1200.0f));
    }

    public void HitStop()
    {
        rigidbody2D.velocity = new Vector2(0.0f, 0.0f);
    }

    void OnDestroy()
    {
        foreach (var val in disposables)
        {
            val.Dispose();
        }
        disposables.Clear();
    }
}
