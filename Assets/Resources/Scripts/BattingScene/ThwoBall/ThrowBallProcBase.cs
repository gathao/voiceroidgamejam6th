﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowBallProcBase
{
    protected Rigidbody2D rigidbody2D;

    protected Transform transform;

    protected GameObject sprites;

    public void Setup(ThrowBall throwBall)
    {
        this.sprites = throwBall.sprites;
        this.rigidbody2D = throwBall.rigidbody2D;
        this.transform = throwBall.transform;
    }

    protected void PositionReset()
    {
        rigidbody2D.velocity = new Vector2(0.0f, 0.0f);
        transform.SetPosition(0.0f, 1.8f, 0.0f);
        transform.SetLocalScale(0.47f, 0.47f, 1.0f);
    }

    public virtual void RandomThrow() { }
}
