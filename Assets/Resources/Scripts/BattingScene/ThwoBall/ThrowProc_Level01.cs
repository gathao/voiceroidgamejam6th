﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ThrowProc_Level01 : ThrowBallProcBase
{
    public override void RandomThrow()
    {
        var kyuusyu = Random.Range(0, 3);

        switch (kyuusyu)
        {
            case 0:
                Straight();
                break;
            case 1:
                LeftCurve();
                break;
            case 2:
                RightCurve();
                break;
        }
    }

    public void Straight(float speed = 1.0f, float rad = 0.0f)
    {
        PositionReset();

        var vecX = Random.Range(-18.0f, 18.0f);

        rigidbody2D.AddForce(new Vector2(vecX, -250.0f));
    }

    public void RightCurve(float speed = 1.0f, float rad = 0.0f)
    {
        PositionReset();

        var vecX01 = Random.Range(3.0f, 4.0f);
        var vecX02 = Random.Range(-5.0f, -4.0f);

        rigidbody2D.velocity = new Vector2(vecX01, -5.0f);
        DOTween.To(() => rigidbody2D.velocity.x, (val) => rigidbody2D.velocity = new Vector2(val, rigidbody2D.velocity.y), vecX02, 2.0f);
    }

    public void LeftCurve(float speed = 1.0f, float rad = 0.0f)
    {
        PositionReset();

        var vecX01 = Random.Range(-3.0f, -4.0f);
        var vecX02 = Random.Range(5.0f, 4.0f);

        rigidbody2D.velocity = new Vector2(vecX01, -5.0f);
        DOTween.To(() => rigidbody2D.velocity.x, (val) => rigidbody2D.velocity = new Vector2(val, rigidbody2D.velocity.y), vecX02, 2.0f);
    }
}
