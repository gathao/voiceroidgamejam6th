﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ThrowProc_Level03 : ThrowBallProcBase
{
    public override void RandomThrow()
    {
        var kyuusyu = Random.Range(0, 5);

        switch (kyuusyu)
        {
            case 0:
                Straight();
                break;
            case 1:
                Invisible();
                break;
            case 2:
                Kasoku();
                break;
            case 3:
                Slider();
                break;
            case 4:
                Sinker();
                break;
        }
    }

    void Invisible(float speed = 1.0f, float rad = 0.0f)
    {
        PositionReset();

        var vecX = Random.Range(-32.0f, 32.0f);

        DOTween.Sequence()
            .OnStart(() =>
            {
                rigidbody2D.AddForce(new Vector2(vecX, -430.0f));
            })
            .AppendInterval(0.11f)
            .AppendCallback(() =>
            {
                sprites.SetActive(false);
            })
            .AppendInterval(1.5f)
            .AppendCallback(() =>
            {
                sprites.SetActive(true);
            })
            .Play();
    }

    void Kasoku()
    {
        PositionReset();

        var vecX = Random.Range(-1.7f, 1.7f);
        var vecY = -5.0f;

        rigidbody2D.velocity = new Vector2(vecX, vecY);

        DOTween.Sequence()
            .OnStart(() =>
            {
                DOTween.To(() => 1.0f,
                    (val) => rigidbody2D.velocity = new Vector2(vecX * val, vecY * val),
                    0.0f, 0.4f);
            })
            .AppendInterval(0.5f)
            .AppendCallback(() =>
            {
                DOTween.To(() => 0.0f,
                    (val) => rigidbody2D.velocity = new Vector2(val * vecX, val * (-10.0f)),
                    1.0f, 0.5f);
            })
            .Play();
    }

    void Straight(float speed = 1.0f, float rad = 0.0f)
    {
        PositionReset();

        var vecX = Random.Range(-14.0f, 14.0f);
        rigidbody2D.AddForce(new Vector2(vecX, -450.0f));
    }

    void Slider(float speed = 1.0f, float rad = 0.0f)
    {
        PositionReset();

        var vecX01 = Random.Range(6.3f, 6.5f);

        DOTween.Sequence()
            .OnStart(() =>
            {
                rigidbody2D.velocity = new Vector2(0.0f, -8.0f);
            })
            .AppendInterval(0.08f)
            .AppendCallback(() =>
            {
                DOTween.To(() => rigidbody2D.velocity.x, (val) => rigidbody2D.velocity = new Vector2(val, rigidbody2D.velocity.y), vecX01, 2.0f);
            })
        .Play();        
    }

    void Sinker(float speed = 1.0f, float rad = 0.0f)
    {
        PositionReset();

        var vecX01 = Random.Range(-5.2f, -5.3f);

        DOTween.Sequence()
            .OnStart(() =>
            {
                rigidbody2D.velocity = new Vector2(0.0f, -8.0f);
            })
            .AppendInterval(0.08f)
            .AppendCallback(() =>
            {
                DOTween.To(() => rigidbody2D.velocity.x, (val) => rigidbody2D.velocity = new Vector2(val, rigidbody2D.velocity.y), vecX01, 2.0f);
            })
        .Play();
    }
}
