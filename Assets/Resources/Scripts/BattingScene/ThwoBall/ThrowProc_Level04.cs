﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ThrowProc_Level04 : ThrowBallProcBase
{
    public override void RandomThrow()
    {
        var kyuusyu = Random.Range(0, 3);

        switch (kyuusyu)
        {
            case 0:
                Shoot();
                break;
            case 1:
                Straight();
                break;
            case 2:
                WideWhiteBall();
                break;
            case 3:
                Slider();
                break;
        }
    }

    void WideWhiteBall()
    {
        PositionReset();

        float interbalTime = 0.08f;

        var vecX01 = Random.Range(15.1f, 15.0f);
        var vecX02 = Random.Range(-20.0f, -20.0f);
        var vecX03 = Random.Range(20.0f, 20.0f);
        var vecX04 = Random.Range(-20.0f, -20.0f);
        var vecX05 = Random.Range(20.0f, 20.0f);
        var vecX06 = Random.Range(-20.0f, -20.0f);
        var vecX07 = Random.Range(20.0f, 20.0f);
        var vecX08 = Random.Range(-20.0f, -20.0f);
        var vecX09 = Random.Range(20.0f, 20.0f);
        var vecX10 = Random.Range(-20.0f, -20.0f);
        var vecX11 = Random.Range(20.0f, 20.0f);
        var vecX12 = Random.Range(-20.0f, -20.0f);
        var vecX13 = Random.Range(20.0f, 20.0f);

        rigidbody2D.AddForce(new Vector2(0.0f, -300.0f));

        DOTween.Sequence()
            .OnStart(() => 
            {
                rigidbody2D.velocity = new Vector2(vecX01, rigidbody2D.velocity.y);
            })
            .AppendInterval(interbalTime)
            .AppendCallback(() =>
            {
                rigidbody2D.velocity = new Vector2(vecX02, rigidbody2D.velocity.y);
            })
            .AppendInterval(interbalTime)
            .AppendCallback(() =>
            {
                rigidbody2D.velocity = new Vector2(vecX03, rigidbody2D.velocity.y);
            })
            .AppendInterval(interbalTime)
            .AppendCallback(() =>
            {
                rigidbody2D.velocity = new Vector2(vecX04, rigidbody2D.velocity.y);
            })
            .AppendInterval(interbalTime)
            .AppendCallback(() =>
            {
                rigidbody2D.velocity = new Vector2(vecX05, rigidbody2D.velocity.y);
            })
            .AppendInterval(interbalTime)
            .AppendCallback(() =>
            {
                rigidbody2D.velocity = new Vector2(vecX06, rigidbody2D.velocity.y);
            })
            .AppendInterval(interbalTime)
            .AppendCallback(() =>
            {
                rigidbody2D.velocity = new Vector2(vecX07, rigidbody2D.velocity.y);
            })
            .AppendInterval(interbalTime)
            .AppendCallback(() =>
            {
                rigidbody2D.velocity = new Vector2(vecX08, rigidbody2D.velocity.y);
            })
            .AppendInterval(interbalTime)
            .AppendCallback(() =>
            {
                rigidbody2D.velocity = new Vector2(vecX09, rigidbody2D.velocity.y);
            })
            .AppendInterval(interbalTime)
            .AppendCallback(() =>
            {
                rigidbody2D.velocity = new Vector2(vecX10, rigidbody2D.velocity.y);
            })
            .AppendInterval(interbalTime)
            .AppendCallback(() =>
            {
                rigidbody2D.velocity = new Vector2(vecX11, rigidbody2D.velocity.y);
            })
            .AppendInterval(interbalTime)
            .AppendCallback(() =>
            {
                rigidbody2D.velocity = new Vector2(vecX12, rigidbody2D.velocity.y);
            })
            .AppendInterval(interbalTime)
            .AppendCallback(() =>
            {
                rigidbody2D.velocity = new Vector2(vecX13, rigidbody2D.velocity.y);
            })
            .Play();
    }

    void Straight(float speed = 1.0f, float rad = 0.0f)
    {
        PositionReset();

        var vecX = Random.Range(-115.0f, 115.0f);
        rigidbody2D.AddForce(new Vector2(vecX, -570.0f));
    }

    void Shoot(float speed = 1.0f, float rad = 0.0f)
    {
        PositionReset();

        var vecX01 = Random.Range(4.5f, 4.0f);
        var vecX02 = Random.Range(-9.0f, -8.5f);

        rigidbody2D.velocity = new Vector2(vecX01, -8.5f);
        DOTween.To(() => rigidbody2D.velocity.x, (val) => rigidbody2D.velocity = new Vector2(val, rigidbody2D.velocity.y), vecX02, 2.0f);
    }

    void Slider(float speed = 1.0f, float rad = 0.0f)
    {
        PositionReset();

        var vecX01 = Random.Range(6.3f, 6.5f);

        DOTween.Sequence()
            .OnStart(() =>
            {
                rigidbody2D.velocity = new Vector2(0.0f, -8.0f);
            })
            .AppendInterval(0.08f)
            .AppendCallback(() =>
            {
                DOTween.To(() => rigidbody2D.velocity.x, (val) => rigidbody2D.velocity = new Vector2(val, rigidbody2D.velocity.y), vecX01, 2.0f);
            })
        .Play();
    }

    void Sinker(float speed = 1.0f, float rad = 0.0f)
    {
        PositionReset();

        var vecX01 = Random.Range(-5.2f, -5.3f);

        DOTween.Sequence()
            .OnStart(() =>
            {
                rigidbody2D.velocity = new Vector2(0.0f, -8.0f);
            })
            .AppendInterval(0.08f)
            .AppendCallback(() =>
            {
                DOTween.To(() => rigidbody2D.velocity.x, (val) => rigidbody2D.velocity = new Vector2(val, rigidbody2D.velocity.y), vecX01, 2.0f);
            })
        .Play();
    }
}
