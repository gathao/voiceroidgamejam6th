﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ThrowProc_Level02 : ThrowBallProcBase
{
    public override void RandomThrow()
    {
        var kyuusyu = Random.Range(0, 3);

        switch (kyuusyu)
        {
            case 0:
                Straight();
                break;
            case 1:
                SCurve();
                break;
            case 2:
                LeftCurve();
                break;
        }
    }

    void Straight(float speed = 1.0f, float rad = 0.0f)
    {
        PositionReset();

        var vecX = Random.Range(-14.0f, 14.0f);

        rigidbody2D.AddForce(new Vector2(vecX, -550.0f));
    }

    void SCurve()
    {
        PositionReset();

        var vecX01 = Random.Range(-25.0f, -35.0f);
        var vecX02 = Random.Range(3.0f, 2.5f);
        var vecX03 = Random.Range(-4.0f, -5.0f);
        var vecX04 = Random.Range(4.0f, 5.0f);

        DOTween.Sequence()
            .OnStart(() =>
            {
                rigidbody2D.AddForce(new Vector2(vecX01, -380.0f));
                DOTween.To(() => rigidbody2D.velocity.x, (val) => rigidbody2D.velocity = new Vector2(val, rigidbody2D.velocity.y), vecX02, 0.1f);
            })
            .AppendInterval(0.1f)
            .AppendCallback(() =>
            {
                DOTween.To(() => rigidbody2D.velocity.x, (val) => rigidbody2D.velocity = new Vector2(val, rigidbody2D.velocity.y), vecX03, 0.6f);
            })
            .AppendInterval(0.6f)
            .AppendCallback(() =>
            {
                DOTween.To(() => rigidbody2D.velocity.x, (val) => rigidbody2D.velocity = new Vector2(val, rigidbody2D.velocity.y), vecX04, 0.6f);
            })
            .Play();
    }

    void LeftCurve(float speed = 1.0f, float rad = 0.0f)
    {
        PositionReset();

        var vecX01 = Random.Range(-1.5f, -2.0f);
        var vecX02 = Random.Range(4.0f, 3.0f);

        rigidbody2D.velocity = new Vector2(vecX01, -8.0f);
        DOTween.To(() => rigidbody2D.velocity.x, (val) => rigidbody2D.velocity = new Vector2(val, rigidbody2D.velocity.y), vecX02, 2.0f);
    }
}
