﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using UniRx.Triggers;
using DG.Tweening;


enum PlayerAnimState
{
    Wait,
    Swing,
}

public class BattingPlayerController : MonoBehaviour
{
    [SerializeField]
    BattingSceneManager battingSceneManager = null;

    [SerializeField]
    Animator playerAnimator = null;

    [SerializeField]
    Collider2D battCollider = null;

    PlayerAnimState currentAnimState = PlayerAnimState.Wait;

    Coroutine swingAnimCroutine = null;

    public bool IsPlayable { get; set; } = true;

    [SerializeField]
    BoundingArea mouseMoveBounding = null;

    [SerializeField]
    public float mouseSensitivity = 0.26f;

    [SerializeField]
    Camera mainCamera = null;

    [SerializeField]
    Collider2D pauseButtonCollider = null;

    [SerializeField]
    PauseManager pauseManager = null;

    List<IDisposable> disposables = new List<IDisposable>();

    List<Sequence> sequences = new List<Sequence>();

    void Start()
    {
        LiftAnimation();
        battCollider.gameObject.SetActive(false);

        Observable
            .EveryUpdate()
            .Where(_ => battingSceneManager != null && battingSceneManager.IsBattingScene)
            .Where(_ => !pauseManager.Pausing)
            .Where(_ => !pauseButtonCollider.OverlapPoint(mainCamera.ScreenToWorldPoint(Input.mousePosition)))
            .Where(_ => !battingSceneManager.IsGameSet)
            .Where(_ => IsPlayable)
            .Where(_ => Input.GetMouseButtonDown(0))
            .Subscribe(_ =>
            {
                SwingAnimation();
            })
            .AddTo(disposables);

        Observable
            .EveryUpdate()
            .Where(_ => battingSceneManager != null && battingSceneManager.IsBattingScene)
            .Where(_ => !pauseManager.Pausing)
            .Where(_ => !battingSceneManager.IsGameSet)
            .Where(_ => IsPlayable)
            .Subscribe(_ =>
            {
                MouseFollowMove();
            })
            .AddTo(disposables);
    }

    public void LiftAnimation()
    {
        IsPlayable = false;

        var listSequence = DOTween.Sequence()
            .OnStart(() =>
            {
                playerAnimator.SetInteger("AnimIdx", 7);
            })
            .AppendInterval(0.5f)
            .AppendCallback(() =>
            {
                IsPlayable = true;
                WaitAnimation();
            })
            .Play();

        sequences.Add(listSequence);
    }

    void WaitAnimation()
    {
        if (swingAnimCroutine != null)
        {
            StopCoroutine(swingAnimCroutine);
            swingAnimCroutine = null;
        }

        IsPlayable = true;
        currentAnimState = PlayerAnimState.Wait;
        playerAnimator.SetInteger("AnimIdx", 0);
    }

    void SwingAnimation()
    {
        if (currentAnimState != PlayerAnimState.Wait)
        {
            return;
        }

        IsPlayable = false;
        currentAnimState = PlayerAnimState.Swing;
        AudioManager.PlaySe(GameSE.Swing);
        swingAnimCroutine = StartCoroutine(SwingAnim());
    }

    IEnumerator SwingAnim()
    {
        playerAnimator.SetInteger("AnimIdx", 1);
        yield return CoroutineUtill.WaitForFrames(2);

        playerAnimator.SetInteger("AnimIdx", 2);
        yield return CoroutineUtill.WaitForFrames(1);

        playerAnimator.SetInteger("AnimIdx", 3);
        battCollider.gameObject.SetActive(true);
        yield return CoroutineUtill.WaitForFrames(6);

        if (TimeManager.Instance.isSlowDown)
        {
            yield return new WaitForSeconds(0.1f);
        }

        playerAnimator.SetInteger("AnimIdx", 4);
        battCollider.gameObject.SetActive(false);
        yield return CoroutineUtill.WaitForFrames(1);

        playerAnimator.SetInteger("AnimIdx", 5);
        yield return CoroutineUtill.WaitForFrames(1);

        if (battingSceneManager.IsCriticalHit)
        {
            playerAnimator.SetInteger("AnimIdx", 8);
            yield return new WaitForSeconds(1.0f);
        }
        else
        {
            playerAnimator.SetInteger("AnimIdx", 6);
            yield return new WaitForSeconds(0.2f);
        }

        playerAnimator.SetInteger("AnimIdx", 5);
        yield return CoroutineUtill.WaitForFrames(3);

        playerAnimator.SetInteger("AnimIdx", 4);
        yield return CoroutineUtill.WaitForFrames(3);

        playerAnimator.SetInteger("AnimIdx", 3);
        yield return CoroutineUtill.WaitForFrames(3);

        playerAnimator.SetInteger("AnimIdx", 1);
        yield return CoroutineUtill.WaitForFrames(3);

        playerAnimator.SetInteger("AnimIdx", 0);

        WaitAnimation();
    }

    void MouseFollowMove()
    {
        float mouseDx = Input.GetAxis("Mouse X") * mouseSensitivity;
        float mouseDy = Input.GetAxis("Mouse Y") * mouseSensitivity;

        var targetPos = new Vector3(transform.position.x, transform.position.y, transform.position.z);
        targetPos.x = Mathf.Clamp(targetPos.x + mouseDx, mouseMoveBounding.MinX, mouseMoveBounding.MaxX);
        targetPos.y = Mathf.Clamp(targetPos.y + mouseDy, mouseMoveBounding.MinY, mouseMoveBounding.MaxY);
        targetPos.z = 0.0f;

        transform.position = Vector3.Lerp(transform.position, targetPos, 0.8f);
    }

    void OnDestroy()
    {
        if (swingAnimCroutine != null)
        {
            StopCoroutine(swingAnimCroutine);
            swingAnimCroutine = null;
        }

        foreach (var val in disposables)
        {
            val.Dispose();
        }
        disposables.Clear();

        foreach (var seq in sequences)
        {
            seq.Kill();
        }
        sequences.Clear();
    }
}
