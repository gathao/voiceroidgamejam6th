﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using UnityEngine.UI;

public class DebugButtons : MonoBehaviour
{
    [SerializeField]
    Button hitButton = null;

    [SerializeField]
    Button fauleButton = null;

    [SerializeField]
    Button homerunButton = null;

    [SerializeField]
    Button gwaragoaButton = null;

    [SerializeField]
    Button playballButton = null;

    [SerializeField]
    Button strikeButton = null;

    [SerializeField]
    BattingFonts battingFonts = null;

    [SerializeField]
    Button throwButton = null;

    [SerializeField]
    PitcherController pitcherController = null;

    void Start()
    {
        hitButton.onClick
            .AsObservable()
            .Subscribe(_ =>
            {
                Debug.Log("hitButton");
                battingFonts.Hit();
            });

        fauleButton.onClick
            .AsObservable()
            .Subscribe(_ =>
            {
                Debug.Log("fauleButton");
                battingFonts.Faule();
            });

        homerunButton.onClick
            .AsObservable()
            .Subscribe(_ =>
            {
                Debug.Log("homerunButton");
                battingFonts.Homerun();
            });

        gwaragoaButton.onClick
            .AsObservable()
            .Subscribe(_ =>
            {
                Debug.Log("gwaragoaButton");
                battingFonts.Gwaragowagakin();
            });

        playballButton.onClick
            .AsObservable()
            .Subscribe(_ =>
            {
                Debug.Log("playballButton");
                battingFonts.Playball();
            });

        strikeButton.onClick
            .AsObservable()
            .Subscribe(_ =>
            {
                Debug.Log("strikeButton");
                battingFonts.Strike();
            });

        throwButton.onClick
            .AsObservable()
            .Subscribe(_ =>
            {
                Debug.Log("throwButton");
                pitcherController.Throw();
            });
    }
}
