﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UniRx;
using UnityConstants;

public class TitleSceneManager : MonoBehaviour
{
    [SerializeField]
    Button startButton = null;

    [SerializeField]
    FadeUIItems fadeUIItems = null;

    List<IDisposable> disposables = new List<IDisposable>();

    void Start()
    {
        AudioManager.PlayBgm(GameBGM.Title);

        startButton.onClick.AsObservable()
            .Subscribe(_ =>
            {
                // ステージセレクトへ
                AudioManager.PlaySe(GameSE.CursorEnter);

                fadeUIItems.FadeOutAll();

                AppManager.Instance.ChangeScene(SceneNames.StageSelectScene);
            })
            .AddTo(disposables);
    }

    void OnDestroy()
    {
        foreach (var val in disposables)
        {
            val.Dispose();
        }
        disposables.Clear();
    }
}
