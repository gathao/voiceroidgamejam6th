﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UniRx;
using UnityConstants;
using DG.Tweening;

public class StageSelectManager : MonoBehaviour
{
    [SerializeField]
    Button returnButton = null;

    [SerializeField]
    LevelSelectButton level01Button = null;

    [SerializeField]
    LevelSelectButton level02Button = null;

    [SerializeField]
    LevelSelectButton level03Button = null;

    [SerializeField]
    LevelSelectButton level04Button = null;

    [SerializeField]
    BattingStartButton battingStartButton = null;

    [SerializeField]
    Button startButton;

    [SerializeField]
    RectTransform batterTatie = null;

    [SerializeField]
    RectTransform pitcherTatie = null;

    [SerializeField]
    FadeUIItems fadeUIItems = null;

    [SerializeField]
    DifficultInfo difficultInfo = null;

    List<IDisposable> disposables = new List<IDisposable>();

    List<Sequence> sequences = new List<Sequence>();

    AppManager appManager;

    SaveManager saveManager;

    PitcherLevel currentLevel = PitcherLevel.None;

    void Start()
    {
        appManager = AppManager.Instance;
        saveManager = SaveManager.Instance;
        AudioManager.PlayBgm(GameBGM.Title);

        level01Button.ChangeActive(true);
        level02Button.ChangeActive(false);
        level03Button.ChangeActive(false);
        level04Button.ChangeActive(false);

        Observable
            .EveryFixedUpdate()
            .Skip(1)
            .Take(1)
            .Subscribe(_ =>
            {
                pitcherTatie.SetLocalPositionX(Screen.width);
                fadeUIItems.FadeOutRight(0.0f, 0.0f);

                // 進捗度に応じてボタンを開放する
                ActivateLevelButtons();
            })
            .AddTo(disposables);

        returnButton.onClick.AsObservable()
            .Subscribe(_ =>
            {
                fadeUIItems.FadeOutAll();
                AudioManager.PlaySe(GameSE.CursorCancel);
                appManager.ChangeScene(SceneNames.TitleScene);
            })
            .AddTo(disposables);

        level01Button.button
            .onClick.AsObservable()
            .Where(_ => currentLevel != PitcherLevel.Level01)
            .Subscribe(_ =>
            {
                level01Button.SelectedColor();
                level02Button.NonSelectColor();
                level03Button.NonSelectColor();
                level04Button.NonSelectColor();

                SelectedLevel(PitcherLevel.Level01);
            })
            .AddTo(disposables);

        level02Button.button
            .onClick.AsObservable()
            .Where(_ => currentLevel != PitcherLevel.Level02)
            .Subscribe(_ =>
            {
                level01Button.NonSelectColor();
                level02Button.SelectedColor();
                level03Button.NonSelectColor();
                level04Button.NonSelectColor();

                SelectedLevel(PitcherLevel.Level02);
            })
            .AddTo(disposables);

        level03Button.button
            .onClick.AsObservable()
            .Where(_ => currentLevel != PitcherLevel.Level03)
            .Subscribe(_ =>
            {
                level01Button.NonSelectColor();
                level02Button.NonSelectColor();
                level03Button.SelectedColor();
                level04Button.NonSelectColor();

                SelectedLevel(PitcherLevel.Level03);
            })
            .AddTo(disposables);

        level04Button.button
            .onClick.AsObservable()
            .Where(_ => currentLevel != PitcherLevel.Level04)
            .Subscribe(_ =>
            {
                level01Button.NonSelectColor();
                level02Button.NonSelectColor();
                level03Button.NonSelectColor();
                level04Button.SelectedColor();

                SelectedLevel(PitcherLevel.Level04);
            })
            .AddTo(disposables);

        startButton.onClick.AsObservable()
            .Where(_ => battingStartButton.IsActive && currentLevel != PitcherLevel.None)
            .Subscribe(_ =>
            {
                var startButtonSequence = DOTween.Sequence()
                    .AppendCallback(() =>
                    {
                        AudioManager.StopBgm();
                        AudioManager.PlaySe(GameSE.CursorEnter);
                        AudioManager.PlaySe(GameSE.GameStart);

                        // 演出
                        fadeUIItems.FadeOutAll();

                        // バッター立ち絵ズーム
                        batterTatie.DOScale(new Vector3(1.2f, 1.2f, 1.0f), 0.5f);
                        batterTatie.DOLocalMove(new Vector3(-350.0f, -140.0f, 0.0f), 0.5f);

                        // ピッチャー立ち絵ズーム
                        pitcherTatie.DOScale(new Vector3(1.2f, 1.2f, 1.0f), 0.5f);
                        pitcherTatie.DOLocalMove(new Vector3(430.0f, -140.0f, 0.0f), 0.5f);

                        battingStartButton.StartAnimation();
                    })
                    .AppendInterval(5.0f)
                    .AppendCallback(() =>
                    {
                        // ゲーム開始
                        appManager.ChangeScene(SceneNames.BattingScene);
                    })
                    .Play();

                sequences.Add(startButtonSequence);
            })
            .AddTo(disposables);
    }

    void ActivateLevelButtons()
    {
        saveManager.Load();
        var playerData = saveManager.playerData;

        switch (playerData.GetCrearedLevel())
        {
            // ステージ1クリア済み
            case PitcherLevel.Level01:
                level01Button.ChangeActive(true);
                level02Button.ChangeActive(true);
                level03Button.ChangeActive(false);
                level04Button.ChangeActive(false);
                break;
            // ステージ2クリア済み
            case PitcherLevel.Level02:
                level01Button.ChangeActive(true);
                level02Button.ChangeActive(true);
                level03Button.ChangeActive(true);
                level04Button.ChangeActive(false);
                break;
            // ステージ3、4クリア済み
            case PitcherLevel.Level03:
            case PitcherLevel.Level04:
                level01Button.ChangeActive(true);
                level02Button.ChangeActive(true);
                level03Button.ChangeActive(true);
                level04Button.ChangeActive(true);
                break;
            default:
                level01Button.ChangeActive(true);
                level02Button.ChangeActive(false);
                level03Button.ChangeActive(false);
                level04Button.ChangeActive(false);
                break;
        }
    }

    void SelectedLevel(PitcherLevel level)
    {
        AudioManager.PlaySe(GameSE.CursorEnter);
        appManager.PitcherLevel = level;
        currentLevel = level;
        difficultInfo.ChangeLevelInfo(currentLevel);
        battingStartButton.HighrightButton();

        // ピッチャーの立ち絵と名前がフェードインする演出
        fadeUIItems.FadeInRight();
        pitcherTatie.SetLocalPositionX(Screen.width);
        pitcherTatie.DOLocalMoveX(500.0f, 0.3f);
    }

    void OnDestroy()
    {
        foreach (var val in disposables)
        {
            val.Dispose();
        }
        disposables.Clear();

        foreach (var seq in sequences)
        {
            seq.Kill();
        }
        sequences.Clear();
    }
}
