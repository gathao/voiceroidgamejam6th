﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EffectName
{
    PickupStar,
    FireExplosion,
    Explosion_Smoke,
    MagicHit,
}

public class EffectController : MonoBehaviour
{
    [SerializeField]
    GameObject PickupStar = null;

    [SerializeField]
    GameObject FireExplosion = null;

    [SerializeField]
    GameObject Explosion_Smoke = null;

    [SerializeField]
    GameObject MagicHit = null;

	Dictionary<EffectName, GameObject> EffectCollection;
    void Start()
    {
		EffectCollection = new Dictionary<EffectName, GameObject>()
		{
			{ EffectName.PickupStar, PickupStar },
			{ EffectName.FireExplosion, FireExplosion },
			{ EffectName.Explosion_Smoke, Explosion_Smoke },
			{ EffectName.MagicHit, MagicHit },
		};
	}

    public void PlayEffect(EffectName effectName, Vector2 position)
    {
		GameObject effectObject = null;
		
		if (EffectCollection.TryGetValue(effectName, out effectObject))
		{
			var particle = SpawnParticle(effectObject);
			particle.transform.position = position;
		}
	}

	private GameObject SpawnParticle(GameObject effectObject)
	{
		GameObject particles = (GameObject)Instantiate(effectObject);
		particles.transform.position = new Vector3(0, particles.transform.position.y, 0);
#if UNITY_3_5
		particles.SetActiveRecursively(true);
#else
		particles.SetActive(true);
#endif

		ParticleSystem ps = particles.GetComponent<ParticleSystem>();

#if UNITY_5_5_OR_NEWER
		if (ps != null)
		{
			var main = ps.main;
			if (main.loop)
			{
				ps.gameObject.AddComponent<CFX_AutoStopLoopedEffect>();
				ps.gameObject.AddComponent<CFX_AutoDestructShuriken>();
			}
		}
#else
		if(ps != null && ps.loop)
		{
			ps.gameObject.AddComponent<CFX_AutoStopLoopedEffect>();
			ps.gameObject.AddComponent<CFX_AutoDestructShuriken>();
		}
#endif
		
		//onScreenParticles.Add(particles);

		return particles;
	}
}
