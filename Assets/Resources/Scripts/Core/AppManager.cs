﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UniRx;
using UniRx.Triggers;
using DG.Tweening;
using UnityConstants;

public enum SceneState
{
    Title,
    Map,
    Game,
    Demo,
}

public class AppManager : SingletonMonoBehaviour<AppManager>
{
    public static readonly Vector2 WindowResolution = new Vector2(1280, 720);
    [RuntimeInitializeOnLoadMethod]
    static void OnRuntimeMethodLoad()
    {
        Screen.SetResolution((int)WindowResolution.x, (int)WindowResolution.y, false, 60);
    }

    //public SceneState CurrentSceneState = SceneState.Title;

    public string CurrentStageName = SceneNames.TitleScene;

    public PitcherLevel PitcherLevel = PitcherLevel.Level01;

    public BattingInfo LastBattingInfo = new BattingInfo();

    protected override void Awake()
    {
        base.Awake();
        DOTween.Init();

        // マウスカーソルを非表示に
        //Cursor.visible = false;

        // セーブデータの保存先フォルダを最初に指定
        // Application.persistentDataPath はAwakeのタイミングでしか呼べないらしい

        //this.UpdateAsObservable()
        //    .Take(1)
        //    .Subscribe(_ =>
        //    {
        //        //onSceneLoaded.OnNext(Unit.Default);
        //    });

        //SaveDataManager.Instance.TryLoad();
        //SaveDataManager.Instance.TryConfigLoad();
    }

    void Start()
    {
        // 現在読み込んでいるシーンの一覧を取得
        var scenes = Enumerable.Range(0, SceneManager.sceneCount).Select(idx => SceneManager.GetSceneAt(idx));

        // 読み込みシーンが1つのみ＝AppBaseSceneしか読み込んでいない場合＝EXE起動した場合
        if (scenes.Count(s => s.isLoaded) == 1)
        {
            // タイトルシーンに飛ばす
            SceneManager
                .LoadSceneAsync(SceneNames.TitleScene, LoadSceneMode.Additive)
                .AsObservable()
                .Where(op => op.isDone)
                .Subscribe(__ =>
                {
                    CurrentStageName = SceneNames.TitleScene;
                    //CurrentSceneState = SceneState.Title;
                });
        }
    }

    IObservable<AsyncOperation> UnloadScene(string sceneName)
    {
        return SceneManager.UnloadSceneAsync(sceneName).AsObservable();
    }
    IObservable<AsyncOperation> LoadScene(string sceneName)
    {
        return SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive).AsObservable();
    }

    public void ChangeScene(string stageName)
    {
        FadeInOutManager.Instance.FadeOut().Subscribe(_ =>
        {
            SceneManager
            .UnloadSceneAsync(CurrentStageName)
            .AsObservable()
            .SelectMany(LoadScene(stageName))
            .Subscribe(__ =>
            {
                var scene = SceneManager.GetSceneByName(stageName);
                SceneManager.SetActiveScene(scene);

                CurrentStageName = stageName;

                FadeInOutManager.Instance.FadeIn().Subscribe(___ =>
                {
                    SceneManager.SetActiveScene(scene);
                });
            });

        });
    }

    public void ReloadScene(string stageName)
    {
        FadeInOutManager.Instance.FadeOut().Subscribe(_ =>
        {
            SceneManager
            .UnloadSceneAsync(stageName)
            .AsObservable()
            //.SelectMany(LoadScene(SceneNames.DummyScene))
            .SelectMany(LoadScene(stageName))
            .Subscribe(__ =>
            {
                var scene = SceneManager.GetSceneByName(stageName);

                CurrentStageName = stageName;

                FadeInOutManager.Instance.FadeIn().Subscribe(___ =>
                {
                    SceneManager.SetActiveScene(scene);
                });
            });
        });
    }
}