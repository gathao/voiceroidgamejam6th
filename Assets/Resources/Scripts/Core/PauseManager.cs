﻿using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using UnityConstants;

public class PauseManager : MonoBehaviour
{
    public bool Pausing { get; set; } = false;

    [SerializeField]
    Image background = null;

    [SerializeField]
    GameObject pauseMenu = null;

    [SerializeField]
    Button backgroundButton = null;

    [SerializeField]
    Button returnButton = null;

    [SerializeField]
    Button retryButton = null;

    [SerializeField]
    Button retireButton = null;

    Coroutine fadeInPauseMenuCoroutine = null;

    List<IDisposable> disposables = new List<IDisposable>();

    void Start()
    {
        background.gameObject.SetActive(false);
        pauseMenu.SetActive(false);

        // 戻るボタン
        returnButton.onClick.AsObservable()
            .Where(_ => Pausing)
            .Subscribe(_ =>
            {
                AudioManager.PlaySe(GameSE.CursorEnter);
                ResumeAll();
            })
            .AddTo(disposables);

        backgroundButton.onClick.AsObservable()
            .Where(_ => Pausing)
            .Subscribe(_ =>
            {
                AudioManager.PlaySe(GameSE.CursorEnter);
                ResumeAll();
            })
            .AddTo(disposables);

        // 再挑戦ボタン
        retryButton.onClick.AsObservable()
            .Where(_ => Pausing)
            .Subscribe(_ =>
            {
                AudioManager.PlaySe(GameSE.CursorEnter);
                ResumeAll();
                AppManager.Instance.ReloadScene(SceneNames.BattingScene);
            })
            .AddTo(disposables);

        // リタイアボタン
        retireButton.onClick.AsObservable()
            .Where(_ => Pausing)
            .Subscribe(_ =>
            {
                AudioManager.PlaySe(GameSE.CursorEnter);
                ResumeAll();
                AppManager.Instance.ChangeScene(SceneNames.StageSelectScene);
            })
            .AddTo(disposables);
    }

    IEnumerator FadeInPauseMenu()
    {
        int endFrame = 10;

        Vector3 endValue = new Vector3(1.0f, 1.0f, 1.0f);

        float scaleX = 0.0f;
        float scaleY = 0.0f;

        float dx = (endValue.x / (float)endFrame);
        float dy = (endValue.y / (float)endFrame);

        for (int countFrame = 0; countFrame < endFrame; countFrame++)
        {
            scaleX += dx;
            scaleY += dy;
            pauseMenu.transform.SetLocalScale(scaleX, scaleY, 1.0f);

            yield return null;
        }

        pauseMenu.transform.SetLocalScale(1.0f, 1.0f, 1.0f);
    }

    public void PauseAll()
    {
        Pausing = true;
        Time.timeScale = 0.0f;

        background.gameObject.SetActive(true);
        pauseMenu.SetActive(true);

        pauseMenu.transform.SetLocalScale(0.0f, 0.0f, 1.0f);
        fadeInPauseMenuCoroutine = StartCoroutine(FadeInPauseMenu());
    }

    public void ResumeAll()
    {
        Time.timeScale = 1.0f;
        Pausing = false;

        background.gameObject.SetActive(false);
        pauseMenu.SetActive(false);
    }

    void OnDestroy()
    {
        if (fadeInPauseMenuCoroutine != null)
        {
            StopCoroutine(fadeInPauseMenuCoroutine);
        }

        foreach (var val in disposables)
        {
            val.Dispose();
        }
        disposables.Clear();
    }
}
