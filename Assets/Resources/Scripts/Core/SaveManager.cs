﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public static class SaveDataKeys
{
    public static string PlayerData = "PlayerData";
}


public class SaveManager : SingletonMonoBehaviour<SaveManager>
{
    public PlayerData playerData;

    void Start()
    {
        Load();
    }

    public void Load()
    {
        if (PlayerPrefs.HasKey(SaveDataKeys.PlayerData))
        {
            var data = PlayerPrefs.GetString(SaveDataKeys.PlayerData);
            playerData = JsonUtility.FromJson<PlayerData>(data);
        }
        else
        {
            playerData = new PlayerData();
        }
    }

    public void Save()
    {
        PlayerPrefs.SetString(SaveDataKeys.PlayerData, playerData.GetJsonData());
    }
}
