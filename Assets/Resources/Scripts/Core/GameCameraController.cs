﻿using System.Linq;
using System;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using UniRx.Triggers;
using Random = UnityEngine.Random;

public enum CameraMode
{
    Fixed,
    Follow,
    Demo
}

[RequireComponent(typeof(Camera))]
public class GameCameraController : MonoBehaviour
{
    [SerializeField]
    private Camera attackedCamera = null;

    [SerializeField]
    public GameObject followTarget;

    [SerializeField]
    private float followTargetCorrectionY = 1f;

    private float shakeDecay = 0.04f;
    private float shakeIntensity;

    public CameraMode Mode { get; private set; }

    [SerializeField]
    public float MinX { get; set; }
    [SerializeField]
    public float MaxX { get; set; }
    [SerializeField]
    public float MinY { get; set; }
    [SerializeField]
    public float MaxY { get; set; }

    public Vector3 BasePosition { get; private set; }

    // カメラが各画面端か否か
    public BoolReactiveProperty IsLeftEdge { get; set; } = new BoolReactiveProperty(false);
    public BoolReactiveProperty IsRightEdge { get; set; } = new BoolReactiveProperty(false);
    public BoolReactiveProperty IsBottomEdge { get; set; } = new BoolReactiveProperty(false);
    public BoolReactiveProperty IsUpEdge { get; set; } = new BoolReactiveProperty(false);

    [SerializeField]
    GameObject LeftEdge = null;

    [SerializeField]
    GameObject RightEdge = null;

    [SerializeField]
    GameObject TopEdge = null;

    [SerializeField]
    GameObject BottomEdge = null;

    List<IDisposable> disposables = new List<IDisposable>();

    private void Awake()
    {
        Mode = CameraMode.Follow;
    }

    private void Start()
    {
        if (LeftEdge != null && RightEdge != null && BottomEdge != null && TopEdge != null)
        {
            MinX = LeftEdge.transform.position.x;
            MaxX = RightEdge.transform.position.x;
            MinY = BottomEdge.transform.position.y;
            MaxY = TopEdge.transform.position.y;
        }

        this.LateUpdateAsObservable()
            .Where(_ => Mode == CameraMode.Follow && followTarget != null)
            .Subscribe(_ =>
            {
                // カメラを遅れて追従させる
                transform.position = Vector3.Lerp(
                    transform.position,
                    new Vector3(followTarget.transform.position.x, followTarget.transform.position.y + followTargetCorrectionY, transform.position.z),
                    5.0f * Time.deltaTime
                );
            })
            .AddTo(disposables);

        // ステージにあわせてスクロール範囲を制御する
        // 左端チェック・補正
        this.LateUpdateAsObservable()
            .Where(_ => Mode == CameraMode.Follow && followTarget != null)
            .Select(_ => attackedCamera.ScreenToWorldPoint(new Vector3(0, 0, 0)))
            //.Where(camLeftPos => camLeftPos.x < MinX)
            .Subscribe(_ => {

                var camPosZero = attackedCamera.ScreenToWorldPoint(new Vector3(0, 0, 0));
                var camPosMax = attackedCamera.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 0));

                // 左端チェック・補正
                if (IsLeftEdge.Value = camPosZero.x <= MinX)
                {
                    attackedCamera.transform.position += new Vector3(MinX - camPosZero.x, 0, 0);
                    BasePosition = attackedCamera.transform.position;
                }
                //右端チェック・補正
                if (IsRightEdge.Value = camPosMax.x >= MaxX)
                {
                    attackedCamera.transform.position -= new Vector3(camPosMax.x - MaxX, 0, 0);
                    BasePosition = attackedCamera.transform.position;
                }
                // 下端チェック・補正
                if (IsBottomEdge.Value = camPosZero.y <= MinY)
                {
                    attackedCamera.transform.position += new Vector3(0, MinY - camPosZero.y, 0);
                    BasePosition = attackedCamera.transform.position;
                }
                // 上端チェック・補正
                if (IsUpEdge.Value = camPosMax.y >= MaxY)
                {
                    attackedCamera.transform.position -= new Vector3(0, camPosMax.y - MaxY, 0);
                    BasePosition = attackedCamera.transform.position;
                }
            })
            .AddTo(disposables);

        // シェイク前かつステージ端補正後のカメラのポジションを記憶しておく
        this.LateUpdateAsObservable()
            //.Where(_ => Mode == CameraMode.Follow && followTarget != null)
            .Subscribe(_ => {
                BasePosition = attackedCamera.transform.position;
            })
            .AddTo(disposables);

        // カメラのシェイク
        this.LateUpdateAsObservable()
            .Where(_ => shakeIntensity > 0)
            .Select(_ => Random.insideUnitSphere * shakeIntensity)
            .Select(randomVec => new Vector3(randomVec.x, randomVec.y, 0))
            .Subscribe(randomVec => {
                transform.position += randomVec;
                shakeIntensity -= shakeDecay;
            })
            .AddTo(disposables);
    }

    public void SetFixedMode()
    {
        Mode = CameraMode.Fixed;
        followTarget = null;
    }

    /// <summary>
    /// デモモードに切り替え
    /// </summary>
    public void SetDemoMode()
    {
        Mode = CameraMode.Demo;
        followTarget = null;
    }

    /// <summary>
    /// フォローモードに切り替え
    /// </summary>
    /// <param name="target"></param>
    public void SetFollowMode()
    {
        Mode = CameraMode.Follow;
        //followTarget = PlayerCharacterManager.Instance.Player.gameObject;
    }
    /// <summary>
    /// フォローモードに切り替え（対象を指定）
    /// </summary>
    /// <param name="target"></param>
    public void SetFollowMode(GameObject target)
    {
        Mode = CameraMode.Follow;
        followTarget = target;
    }

    public void Shake(float power = 0.3f)
    {
        shakeIntensity = power;
    }
    
    public void ResetPosition()
    {
        transform.position = new Vector3(0.0f, 0.0f, -10.0f);
    }

    void OnDestroy()
    {
        foreach (var val in disposables)
        {
            val.Dispose();
        }
        disposables.Clear();
    }
}
