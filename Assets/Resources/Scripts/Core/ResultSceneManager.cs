﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UniRx;
using UnityConstants;
using DG.Tweening;

public class ResultSceneManager : MonoBehaviour
{
    [SerializeField]
    LevelText levelText = null;

    [SerializeField]
    BattingInfo battingInfo = null;

    [SerializeField]
    Image NormaClearText = null;

    [SerializeField]
    Image NormaFailedText = null;

    [SerializeField]
    FadeUIItems fadeUIItems = null;

    [SerializeField]
    Button retryButton = null;

    [SerializeField]
    Button completeButton = null;

    [SerializeField]
    GameObject targetHomerun = null;

    [SerializeField]
    GameObject homerunCount = null;

    [SerializeField]
    GameObject targetHikyori = null;

    [SerializeField]
    GameObject hikyori = null;

    AppManager appManager;

    SaveManager saveManager;

    List<IDisposable> disposables = new List<IDisposable>();

    List<Sequence> sequences = new List<Sequence>();

    bool isHomerunClear = false;

    bool isHikyoriClear = false;

    void Start()
    {
        appManager = AppManager.Instance;
        saveManager = SaveManager.Instance;

        AudioManager.PlayBgm(GameBGM.Muon);
        AudioManager.PlaySe(GameSE.Victory);

        Observable
            .EveryFixedUpdate()
            .Skip(1)
            .Take(1)
            .Subscribe(_ =>
            {
                levelText.ChangeLevelText(appManager.PitcherLevel);
            })
            .AddTo(disposables);

        var resultSeq = DOTween.Sequence()
            .OnStart(() =>
            {
            })
            .AppendInterval(0.3f)
            .AppendCallback(() =>
            {
                fadeUIItems.FadeInAll(0.3f);
            })
            .AppendInterval(1.0f)
            .AppendCallback(() =>
            {
                // ホームラン数ノルマクリア判定
                targetHomerun.SetActive(true);
                homerunCount.SetActive(true);

                isHomerunClear = appManager.LastBattingInfo.HomerunCount >= appManager.LastBattingInfo.TargetHomerun;
                battingInfo.TargetHomerun = appManager.LastBattingInfo.TargetHomerun;
                battingInfo.HomerunCount = appManager.LastBattingInfo.HomerunCount;
            })
            .AppendInterval(1.0f)
            .AppendCallback(() =>
            {
                // 飛距離ノルマクリア判定
                targetHikyori.SetActive(true);
                hikyori.SetActive(true);

                isHikyoriClear = appManager.LastBattingInfo.Hikyori >= appManager.LastBattingInfo.TargetHikyori;
                battingInfo.TargetHikyori = appManager.LastBattingInfo.TargetHikyori;
                battingInfo.Hikyori = appManager.LastBattingInfo.Hikyori;

            })
            .AppendInterval(1.0f)
            .AppendCallback(() =>
            {
                // ステージクリア
                if (isHomerunClear && isHikyoriClear)
                {
                    AudioManager.PlaySe(GameSE.Result_Homerun);

                    // ノルマクリア成功の文字表示
                    NormaClearText.gameObject.SetActive(true);
                    NormaClearText.rectTransform.pivot = new Vector2(0.5f, -5.0f);
                    NormaClearText.rectTransform.DOPivotY(0.5f, 0.5f);

                    if (saveManager.playerData.GetCrearedLevel() < appManager.PitcherLevel)
                    {
                        saveManager.playerData.SetCrearedLevel(appManager.PitcherLevel);
                        saveManager.Save();
                    }
                }
                else
                {
                    AudioManager.PlaySe(GameSE.Result_Faul);

                    // ノルマクリア失敗の文字表示
                    NormaFailedText.gameObject.SetActive(true);
                    NormaFailedText.rectTransform.pivot = new Vector2(0.5f, -5.0f);
                    NormaFailedText.rectTransform.DOPivotY(0.5f, 0.5f);
                }
            })
            .AppendInterval(0.5f)
            .AppendCallback(() =>
            {
                retryButton.gameObject.SetActive(true);
                completeButton.gameObject.SetActive(true);
            })
            .Play();

        sequences.Add(resultSeq);

        retryButton.onClick
            .AsObservable()
            .Subscribe(_ =>
            {
                AudioManager.PlaySe(GameSE.CursorEnter);
                AppManager.Instance.ChangeScene(SceneNames.BattingScene);
            })
            .AddTo(disposables);

        completeButton.onClick
            .AsObservable()
            .Subscribe(_ =>
            {
                AudioManager.PlaySe(GameSE.CursorEnter);
                AppManager.Instance.ChangeScene(SceneNames.StageSelectScene);
            })
            .AddTo(disposables);
    }

    void OnDestroy()
    {
        foreach (var val in disposables)
        {
            val.Dispose();
        }
        disposables.Clear();

        foreach (var seq in sequences)
        {
            seq.Kill();
        }
        sequences.Clear();
    }
}
