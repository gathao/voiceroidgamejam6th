﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UniRx;
using System;

public class FadeInOutManager : SingletonMonoBehaviour<FadeInOutManager>
{
    [SerializeField]
    FadeImage fadeImage = null;

    [SerializeField]
    Fade fade = null;

    public BoolReactiveProperty IsFadeNow = new BoolReactiveProperty(false);

    public IObservable<Unit> FadeOut(float time = 1)
    {
        //GameInput.Instance.ChangeDemoInput();

        Subject<Unit> subject = new Subject<Unit>();

        IsFadeNow.Value = true;
        fade.FadeIn(time, () =>
        {
            IsFadeNow.Value = false;

            subject.OnNext(Unit.Default);
            subject.OnCompleted();
        });

        return subject.AsObservable();

    }

    public IObservable<Unit> FadeIn(float time = 1, bool changeInput = true)
    {
        //if (changeInput)
        //{
        //    GameInput.Instance.ChangeNormalInput();
        //}

        Subject<Unit> subject = new Subject<Unit>();

        IsFadeNow.Value = true;
        fade.FadeOut(time, () =>
        {
            IsFadeNow.Value = false;

            subject.OnNext(Unit.Default);
            subject.OnCompleted();
        });

        return subject.AsObservable();
    }
}
