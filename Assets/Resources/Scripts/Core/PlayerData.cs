﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class PlayerData
{
    public float maxHikyori = 0.0f;

    public float totalHikyori = 0.0f;

    public PitcherLevel crearedLevel = PitcherLevel.None;

    public float GetMaxHikyori()
    {
        return maxHikyori;
    }

    public void SetMaxHikyori(float hikyori)
    {
        maxHikyori = hikyori;
    }

    public float GetTotalHikyori()
    {
        return totalHikyori;
    }

    public void SetTotalHikyori(float hikyori)
    {
        totalHikyori = hikyori;
    }

    public PitcherLevel GetCrearedLevel()
    {
        return crearedLevel;
    }

    public void SetCrearedLevel(PitcherLevel level)
    {
        crearedLevel = level;
    }

    public string GetNormalData()
    {
        return "MaxHikyori: " + maxHikyori + " TotalHikyori: " + totalHikyori + " CreardLevel: " + crearedLevel;
    }

    public string GetJsonData()
    {
        return JsonUtility.ToJson(this);
    }
}
