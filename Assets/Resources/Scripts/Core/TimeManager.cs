﻿
using UnityEngine;
using System.Collections;

public class TimeManager : SingletonMonoBehaviour<TimeManager>
{
	//　Time.timeScaleに設定する値
	private float timeScale = 0.1f;
	//　時間を遅くしている時間

	private float slowTime = 1f;
	//　経過時間
	private float elapsedTime = 0f;
	//　時間を遅くしているかどうか
	public bool isSlowDown { get; private set; } = false;

	void Update()
	{
		//　スローダウンフラグがtrueの時は時間計測
		if (isSlowDown)
		{
			elapsedTime += Time.unscaledDeltaTime;
			if (elapsedTime >= slowTime)
			{
				SetNormalTime();
			}
		}
	}

	//　時間を遅らせる処理
	public void SlowDown(float timeScale = 0.1f, float slowTime = 1.0f)
	{
		this.timeScale = timeScale;
		this.slowTime = slowTime;

		elapsedTime = 0f;
		Time.timeScale = timeScale;
		isSlowDown = true;
	}

	//　時間を元に戻す処理
	public void SetNormalTime()
	{
		Time.timeScale = 1f;
		isSlowDown = false;
	}
}
