﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoundingArea : MonoBehaviour
{
    [SerializeField]
    GameObject LeftEdge = null;

    [SerializeField]
    GameObject RightEdge = null;

    [SerializeField]
    GameObject TopEdge = null;

    [SerializeField]
    GameObject BottomEdge = null;

    float _minX;
    public float MinX
    {
        get
        {
            SetupBinds();
            return _minX;
        }
        private set
        {
            _minX = value;
        }
    }

    float _maxX;
    public float MaxX
    {
        get
        {
            SetupBinds();
            return _maxX;
        }
        set
        {
            _maxX = value;
        }
    }

    float _minY;
    public float MinY
    {
        get
        {
            SetupBinds();
            return _minY;
        }
        set
        {
            _minY = value;
        }
    }

    float _maxY;
    public float MaxY
    {
        get
        {
            SetupBinds();
            return _maxY;
        }
        set
        {
            _maxY = value;
        }
    }

    bool isInitialised = false;

    void Start()
    {
        SetupBinds();
    }

    public void SetupBinds()
    {
        if(isInitialised)
        {
            return;
        }

        MinX = LeftEdge.transform.position.x;
        MaxX = RightEdge.transform.position.x;
        MinY = BottomEdge.transform.position.y;
        MaxY = TopEdge.transform.position.y;

        isInitialised = true;
    }
}
