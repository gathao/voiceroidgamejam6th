﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class CoroutineUtill
{
    public static IEnumerator WaitForFrames(int frameCount)
    {
        if (frameCount <= 0)
        {
            frameCount = 0;
        }

        while (frameCount > 0)
        {
            frameCount--;
            yield return null;
        }
    }
}
